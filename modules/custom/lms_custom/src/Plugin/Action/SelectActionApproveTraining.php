<?php

namespace Drupal\lms_custom\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Action description.
 *
 * @Action(
 *   id = "select_action_approve_training",
 *   label = @Translation("Approve Training"),
 *   type = "node"
 * )
 */
class SelectActionApproveTraining extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface;
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->messenger     = $container->get('messenger');
    $instance->account       = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Do some processing..
    if ($entity->hasField('field_approved')) {
      $message =  "";
      $entity->field_approved->value = 1;

      $message .= "'  bulk action" . " 'approve training'. ";

      $entity->setNewRevision(TRUE);
      $entity->setRevisionLogMessage($message);
      $entity->setRevisionUserId($this->account->id()); 
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->save();
    }
    $this->messenger->addStatus('Selected trainings are approved successfully.');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityTypeId() == 'node') {
      $access = $object->status->access('edit', $account, TRUE)
        ->andIf($object->access('update', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return $object->access('update', $account, $return_as_object);
  }

}
