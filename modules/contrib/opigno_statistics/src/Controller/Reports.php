<?php

namespace Drupal\opigno_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Statistics user controller.
 */
class Reports extends ControllerBase {

  public function getReports() {
    // Prepare the table header.
    $header = [
      ['data' => $this->t('Name'), 'class' => 'name'],
      ['data' => $this->t('Action'), 'class' => 'name'],
    ];
    
    $table_rows = [];
    $build = [
      '#type' => 'table',
      '#attributes' => [
        // 'class' => ['statistics-table', 'trainings-list'],
        'class' => ['trainings-list'],
      ],
      '#header' => $header,
    ];

    // Details link options.
    $options = [
      'attributes' => [
        'class' => ['action-training-report'],
      ],
    ];

    $options = [
      'attributes' => [
        // 'target' => '_blank',
        'class' => ['action-training-report', 'btn btn-rounded'],
      ],
    ];
    $url_by_training = Url::fromUri('internal:/statistics/dashboard', $options);
    $reports_by_training =  Link::fromTextAndUrl('Click here', $url_by_training)->toString();

    $url_by_members = Url::fromUri('internal:/statistics/statistics-by-members', $options);
    $reports_by_member =  Link::fromTextAndUrl('Click here', $url_by_members)->toString();

    $url_by_class = Url::fromUri('internal:/statistics/statistics-by-class', $options);
    $reports_by_class =  Link::fromTextAndUrl('Click here', $url_by_class)->toString();

    $url_by_external = Url::fromUri('internal:/external-training-report', $options);
    $reports_by_external =  Link::fromTextAndUrl('Click here', $url_by_external)->toString();

    // Build table rows.
    $table_rows[] = [
      ['data' => 'Report by Trainings' , 'class' => 'name'],
      ['data' => $reports_by_training, 'class' => 'name'],
    ];

    $table_rows[] = [
      ['data' => 'Report by Members' , 'class' => 'name'],
      ['data' => $reports_by_member, 'class' => 'name'],
    ];

    $table_rows[] = [
      ['data' => 'Report by Class' , 'class' => 'name'],
      ['data' => $reports_by_class, 'class' => 'name'],
    ];

    $table_rows[] = [
      ['data' => 'External Trainings' , 'class' => 'name'],
      ['data' => $reports_by_external, 'class' => 'name'],
    ];

    return $build + ['#rows' => $table_rows];
  }

}