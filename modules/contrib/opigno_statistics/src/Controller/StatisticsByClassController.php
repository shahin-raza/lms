<?php

namespace Drupal\opigno_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Statistics user controller.
 */
class StatisticsByClassController extends ControllerBase {

  public function index() {

		$query = \Drupal::database()->select('groups', 'g');
    $query->leftJoin('group_content_field_data', 'g_c_f_d', 'g_c_f_d.gid = g.id');
    $query->leftJoin('groups_field_data', 'g_f_d', 'g.id = g_f_d.id');
    $query->fields('g', ['id', 'type']);
    $query->addExpression('COUNT(g_c_f_d.id)', 'members');
    $query->condition('g_c_f_d.type', ['learning_path-group_membership', 'opigno_class-group_membership'] , 'IN');
    $query->condition('g.type', 'opigno_class' , '=');
    $query->groupBy('g_c_f_d.gid');
    $query->groupBy('g_f_d.label');
    $query->fields('g_f_d', ['label'])
    ->orderBy('g.id');   
    $data = $query
      ->execute()
      ->fetchAll();

    // Prepare the table header.
    $header = [
      ['data' => $this->t('Group ID'), 'class' => 'name'],
      ['data' => $this->t('Name'), 'class' => 'name'],
      ['data' => $this->t('Type'), 'class' => 'name'],
      ['data' => $this->t('Members'), 'class' => 'name'],
      ['data' => $this->t('Action'), 'class' => 'name'],
    ];
    
    $table_rows = [];
    $build = [
      '#type' => 'table',
      '#attributes' => [
        'class' => ['trainings-list'],
      ],
      '#header' => $header,
    ];

    // Details link options.
    $options = [
      'attributes' => [
        'class' => ['action-training-report'],
      ],
    ];

    $options = [
      'attributes' => [
        'class' => ['action-training-report', 'btn btn-rounded'],
      ],
    ];

    if (!empty($data)) {
      foreach ($data as $d) {
        $url = "";
        $url = Url::fromRoute('opigno_statistics.class_members_details', [
          'gid' => $d->id,
          ],
        );

        $link = Link::fromTextAndUrl($this->t('View members'), $url)->toRenderable();
        $link['#attributes'] = ['class' => 'btn btn-rounded'];

        // Build table rows.
        $table_rows[] = [
          ['data' => $d->id , 'class' => 'name'],
          ['data' => $d->label, 'class' => 'name'],
          ['data' => !empty($d->type) ? $this->getGroupType($d->type) : $d->type , 'class' => 'name'],
          ['data' => $d->members, 'class' => 'name'],
          ['data' => $link, 'class' => 'name'],
        ];
      }
    }

    return $build + ['#rows' => $table_rows];
  }

  public function getGroupType($type) {
    if (!empty($type)) {
      $type = str_replace("_"," ", $type);
      $type = str_replace("opigno","", $type);
      $type = ucwords($type);
    }

    return $type;
  }

}